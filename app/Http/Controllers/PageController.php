<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index() {
        return view('pages.home');
    }
    public function aboutUs() {
        return view('pages.about-us');
    }
    public function contactUs() {
        return view('pages.contact-us');
    }
    public function gallery() {
        return view('pages.gallery');
    }
    public function services() {
        return view('pages.services');
    }
    public function timing() {
        return view('pages.timing');
    }
}
