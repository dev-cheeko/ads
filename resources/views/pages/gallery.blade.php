@extends('layout')
@section('title')
Gallery
@endsection

@section('body')
<!-- Gallery Section -->
<div id="gallery-section" class="padding-50 gray projects">
  <div class="container">
    <div class="row">
      <div class="col-sm-1 col-lg-2"></div>
      <div class="col-xs-12 col-sm-10 col-lg-8 text-center">
        <h2 class="text-uppercase title-style01">Classes <span class="color_red">Gallery</span></h2>
        <div class="line_1-1"></div>
        <div class="line_2-2"></div>
        <div class="line_3-3"></div>
      </div>
      <div class="col-sm-1 col-lg-2"></div>
    </div>
    <div class="row pt-40 gallery_item">
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery Event1 mix"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/33.jpg")}}" alt="#"> </div>
            <div class="button"> <a class="gallery_img btn" href="{{@asset("images/50033.jpg")}}"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery Event4 mix"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/34.jpg")}}" alt="#"> </div>
           
            <div class="button"> <a class="gallery_img btn" href="{{@asset("images/500/34.jpg")}}"><i class="fa fa-photo"></i></a> </div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery Event2 mix"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/6.jpg")}}" alt="#"> </div>
           
            <div class="button"> <a href="{{@asset("images/500/6.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/2.jpg")}}" alt="#"> </div>
           
            <div class="button"> <a href="{{@asset("images/500/2.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a> </div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/8.jpg")}}" alt="#"> </div>
           
           
            <div class="button"> <a href="{{@asset("images/500/8.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/7.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/7.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/1.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/1.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/3.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/3.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/4.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/4.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/7.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/7.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/9.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/9.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/10.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/10.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/11.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/11.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/12.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/12.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/13.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/13.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/14.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/14.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/15.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/15.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/16.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/16.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/17.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/17.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/18.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/18.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/19.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/19.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/21.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/21.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/22.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/22.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/23.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/23.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/24.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/24.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/25.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/25.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/26.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/26.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/27.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/28.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/29.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/29.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/30.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/30.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/31.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/31.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/32.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/32.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/35.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/35.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/36.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/36.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/37.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/37.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
        <div class="project-single">
          <div class="project-inner">
            <div class="project-head"> <img src="{{@asset("images/500/38.jpg")}}" alt="#"> </div>
          
            <div class="button"> <a href="{{@asset("images/500/38.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
          </div>
        </div>
        <!--/ End Single Project --> 
      </div>
    </div>
  </div>
</div>
  
@endsection