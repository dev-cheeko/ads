@extends('layout')
@section('title') 
    Classes Timing
@endsection
@section('body')
    <section class="timing padding-50 white">
        <div class="container">
            <div class="row py-5">
                <div class="col-lg-12 text-center">
                  <h2 class="text-uppercase title-style01">CLASSES <span class="color_red">Timing</span></h2>
                </div>
            </div>
           <div class="row m-auto">
               <div class="col-md-6">
                <div class="row pb-3">
                  <div class="col-lg-12">
                    <h4 class="text-uppercase">Daily <span class="color_red">Timing</span></h4>
                  </div>
                </div>
                <div class="row">
                    <div class="cold-md-12">
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                               
                                <th scope="col">Classes</th>
                                <th scope="col">Timing</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Yoga Class</td>
                                <td>06:00 AM - 08:00 AM</td>
                              </tr>
                              <tr>
                            
                                <td>AEROBIC & GYMNASTIC</td>
                                <td>08:00 AM -10:00 AM</td>
                              </tr>
                              <tr>
                                
                                <td>BOLLYWOOD</td>
                                <td>10:30 AM - 12:30</td>
                              </tr>
                            </tbody>
                          </table>
                    </div>
                </div>
               </div>
               <div class="col-md-6">
                <div class="row">
                  <div class="col-lg-12">
                    <h4 class="text-uppercase pb-3">Alternate <span class="color_red">Timing</span></h4>
                  </div>
                </div>
                <div class="row">
                    <div class="cold-md-12">
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col">Timing</th>
                                <th scope="col">MWF</th>
                                <th scope="col">TTS</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>03:00 - 04:30 PM</td>
                                <td>FREE STYLE</td>
                                <td>TRADITIONAL</td>
                              </tr>
                              <tr>
                                <td>05:00 - 06:00</td>
                                <td>SALSA</td>
                                <td>BELLY DANCE</td>
                              </tr>
                              <tr>
                                <td>06:30 - 08:00</td>
                                <td>HIP HOP</td>
                                <td>CONTEMPORARY</td>
                              </tr>
                            </tbody>
                          </table>
                    </div>
                </div>
               </div>
           </div>
        </div>
    </section>
@endsection