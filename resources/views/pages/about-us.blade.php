@extends('layout')
@section('title')
About Us
@endsection

@section('body')
<!-- ABOUT US -->
  <section id="about-section" class="padding-50 white">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-sm-12 col-xs-12">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="text-uppercase">Welcome To <span class="color_red">Aditya Dance Studio</span></h2>
              <div class="line_1"></div>
              <div class="line_2"></div>
              <div class="line_3"></div>
            </div>
          </div>
          <div class="about-details">
            <p>Let the language of dance speak through your body. We teach people of all ages in such a way that they can learn in the easiest way. We start with basic and also warm-up regularly for fitness care. We also offer new signature styles for learning with special props and equipment. We call the best faculties from Bollywood to teach a high level and exceptional style.</p>
          </div>
        </div>
        <div class="col-lg-5 col-sm-12 col-xs-12">
          <div class="about-details-img"> <img src="{{asset("images/dance/IMG-20200410-WA0052.jpg")}}" alt="image"> </div>
        </div>
      </div>
    </div>
  </section>
  <section id="cs" class="padding-50 white">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="text-uppercase">Director's <span class="color_red"> Talk</span></h2>
              <div class="line_1"></div>
              <div class="line_2"></div>
              <div class="line_3"></div>
            </div>
            <div class="col-md-7">
              <div class="about-details">
                <p>I started my career as a child artist, worked in several ads and also as a backstage artist. As a child, I danced at school, college and on the street also. Further I received an offer from a person to work in T-series production. In this part of my career, I learned dance from city Patna to film city Mumbai. Then I worked on the Bhojpuri album and choreographed. After that I started doing many shows and programs on different platforms. I also worked in Schools & Colleges as a choreographer. During those days I started to learn classical dance and pursuing dance degree. Then I moved to Mumbai for my further career. Then when my mother's health deteriorated suddenly, I returned to my home and decided to work at my native place. I tried my best to train many students for free and I did. My first work in my native town was regarding Pulwama Attack (Ek Shaam Shaheedon Ke Naam).</p>
                
                <p>I am certified and trained under the "Allahabad University". During my personal and commercial work I meet to famous bollywood choreographer Ganesh Acharya, Saroj Khan, Kaanu Mukherji (Bhojpuri choreographer), Deep Sristh (Director), Shibha Singh (Actress), Manoj Tiwari (Indian politician, singer and actor), Sohail Khan (Actor), Alisha (DID, First Winner), my supportive and take caring teacher Minku Sir (Bollywood Choreographer), Rajeev Shurti (Choreographed in mostly Indian Bollywood Cinema in Wanted, What's your raashee, Fukrey, Dabang and many more), Avinash Sir famous choreographer in Bihar (My first teacher in my native place).</p>
              </div>
            </div>
            <div class="col-lg-5 col-sm-12 col-xs-12">
              <div class="about-details-img"> <img src="{{asset("images/director.png")}}" alt="Adity Kumar Director of Aditya Dance Studio"> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- ABOUT US --> 
@endsection