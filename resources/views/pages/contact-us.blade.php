@extends('layout')
@section('title') 
    Contact Us
@endsection
@section('body')
<!-- Contact Section -->
<div id="contacts" class="padding-50 gray">
  <div class="container">
    <div class="row">
      <div class="col-sm-1 col-lg-2"></div>
      <div class="col-xs-12 col-sm-10 col-lg-8 text-center">
        <h2 class="text-uppercase title-style01">Contact<span class="color_red"> Us</span></h2>
        <div class="line_1-1"></div>
        <div class="line_2-2"></div>
        <div class="line_3-3"></div>
        <p class="heading_space">Fill the form and we wil get back to you as soon as possible </p>
      </div>
      <div class="col-sm-1 col-lg-2"></div>
    </div>
    <div class="row pt-40">
      <div class="col-lg-6 col-sm-12">
        <div class="contact-info">
          <h5>Main Office</h5>
          <div class="contact-inner-box">
            <div class="contact-inner-icon"> <i class="fa fa-map-marker"></i> </div>
            <div class="contact-inner-text"> <span>Main Gate, South of Gandhi Maidan
            </span> <span>Masaurhi, 804452</span> <span>Patna, Bihar
            </span> </div>
          </div>
          <div class="contact-inner-box">
            <div class="contact-inner-icon"> <i class="fa fa-envelope-o" aria-hidden="true"></i> </div>
            <div class="contact-inner-text"> <span>www.adityadancestudio.net.in</span> <span>adityakumar12091981@gmail.com</span> </div>
          </div>
          <div class="contact-inner-box">
            <div class="contact-inner-icon"> <i class="fa fa-phone"></i> </div>
            <div class="contact-inner-text"> <span>+ 91-7488087155 </span> </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="contact-info">
          <h5>Branch Office</h5>
          <div class="contact-inner-box">
            <div class="contact-inner-icon"> <i class="fa fa-map-marker"></i> </div>
            <div class="contact-inner-text"> <span>Street beside the Photo Bharat </span> <span>Station Road, Masaurhi, 804452
            </span> <span>Patna, Bihar</span> </div>
          </div>
          <div class="contact-inner-box">
            <div class="contact-inner-icon"> <i class="fa fa-envelope-o" aria-hidden="true"></i> </div>
            <div class="contact-inner-text"> <span>www.adityadancestudio.net.in</span> <span>adityakumar12091981@gmail.com</span> </div>
          </div>
          <div class="contact-inner-box">
            <div class="contact-inner-icon"> <i class="fa fa-phone"></i> </div>
            <div class="contact-inner-text"> <span>+ 91-7488087155 </span> </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row pt-40">
      <div class="col-lg-12 col-sm-12">
        <div class="contact">
          <form class="form" name="enq" method="post" action="" onSubmit="return validation();">
            <div class="row">
              <div class="form-group col-md-6">
                <input name="name" class="form-control" id="firstname" placeholder="Name" required="required" type="text">
              </div>
              <div class="form-group col-md-6">
                <input name="email" class="form-control" id="email" placeholder="Email" required="required" type="email">
              </div>
              <div class="form-group col-md-12">
                <input name="subject" class="form-control" id="subject" placeholder="Subject" required="required" type="text">
              </div>
              <div class="form-group col-md-12">
                <textarea rows="3" name="message" class="form-control" id="description" placeholder="Your Message" required="required"></textarea>
              </div>
              <div class="col-md-12">
                <div class="actions">
                  <input value="Send message" name="submit" id="submit-Button" class="btn btn-lg btn-con-bg" title="Submit Your Message!" type="submit">
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- Contact Section --> 
@endsection