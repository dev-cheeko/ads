@extends('layout')
@section('title') 
    Aditya Dance Studio
@endsection
@section('body')

<!--Banner section-->
  <div class="sliderM">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="{{@asset('images/5cross.jpg')}}" alt="Aditya Dance Studio">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="{{@asset('images/1.jpg')}}" alt="Aditya Dance Studio">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="{{@asset('images/2.jpg')}}" alt="Aditya Dance Studio">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
  <section id="about-section" class="padding-50 white">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-sm-12 col-xs-12">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="text-uppercase">Welcome To <span class="color_red">Aditya Dance Studio</span></h2>
              <div class="line_1"></div>
              <div class="line_2"></div>
              <div class="line_3"></div>
            </div>
          </div>
          <div class="about-details">
            <p>Let the language of dance speak through your body. We teach people of all ages in such a way that they can learn in the easiest way. We start with basic and also warm-up regularly for fitness care. We also offer new signature styles for learning with special props and equipment. We call the best faculties from Bollywood to teach a high level and exceptional style.
            </p>
            <div class="global_btn"> <a class="btn_one" href="/about-us">Read More</a> <a class="btn_two" href="/contact-us">Contact Us</a> </div>
          </div>
        </div>
        <div class="col-lg-5 col-sm-12 col-xs-12">
          <div class="about-details-img"> <img src="{{asset("images/500/33.jpg")}}" alt="Aditya Kumar  With Sohail Khan"> </div>
        </div>
      </div>
    </div>
  </section>

  <div id="count-down-section" class="padding-50">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-sm-12">
          <div class="counter_area">
            <div class="countDown clearfix">
              <div class="row count_down_bg">
                <div class="col-lg-4 col-sm-6 col-xs-12">
                  <div class="single-count day">
                    <h1 class="days">100+</h1>
                    <p class="days_ref">Awards Won</p>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                  <div class="single-count hour">
                    <h1 class="hours">390</h1>
                    <p class="hours_ref">Hrs of Work/Month</p>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                  <div class="single-count min">
                    <h1 class="minutes">70+ </h1>
                    <p class="minutes_ref">Students</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>

  <div id="blog-section" class="padding-50 white blog-section">
    <div class="container">
      <div class="row">
        <div class="col-sm-1 col-lg-2"></div>
        <div class="col-xs-12 col-sm-10 col-lg-8 text-center">
          <h2 class="text-uppercase title-style01">special <span class="color_red">Classes</span></h2>
          <div class="line_1-1"></div>
          <div class="line_2-2"></div>
          <div class="line_3-3"></div>
        </div>
        <div class="col-sm-1 col-lg-2"></div>
      </div>
      <div class="row pt-40">
        <div class="col-lg-4 col-sm-6 col-xs-12">
          <div class="blog-box text-center">
            <div class="blog-post-images pt-5"> <a href="#"> <img src="images/Asset 1Zumba.svg"  class="img-fluid" width="60px" alt="image"> </a> </div>
            <div class="blogs-post"> 
              <h4>Zumba</h4>
              <p>The Zumba is an exercise fitness program that borrows Latin flavor from the following dance styles: Cumbia, salsa, merengue, mambo, flamenco, cha-cha-cha, Reggaeton, samba, belly dancing, Bhangra, hip-hop, and tango.
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-xs-12">
          <div class="blog-box text-center">
            <div class="blog-post-images pt-5"> <a href="#"> <img src="images/Asset 2Aerobic.svg"  class="img-fluid" width="100px" alt="image"></a> </div>
            <div class="blogs-post">
              <h4>Aerobic</h4>
              <p>Aerobics is a form of physical exercise that combines rhythmic aerobic exercise with stretching and strength training routines with the goal of improving all elements of fitness.
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-xs-12">
          <div class="blog-box text-center">
            <div class="blog-post-images pt-5"> <a href="#"> <img src="images/Asset 3Yoga.svg" class="img-fluid" width="45px"  alt="image"> </a> </div>
            <div class="blogs-post"> 
              <h4>Yoga</h4>
              <p>Helps improve your dance steps. Yoga is a group of physical, mental, and spiritual practices or disciplines which originated in ancient India.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="video-section" class="white video-section container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12 video_sec">
        <div class="vodeo-box">
          <div class="video bg-image text-center" style="background-image: url(images/maxresdefault.jpg);">
            <div class="video__inner"> <a class="" data-toggle="modal" data-target="#video_lightbox"><i class="fa fa-play"></i></a> </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12 video_sec">
        <div class="row pt-40">
          <div class="col-sm-1 col-lg-2"></div>
          <div class="col-xs-12 col-sm-10 col-lg-8 text-center">
            <h2 class="text-uppercase title-style01">Classes <span class="color_red">speciality</span></h2>
            <div class="line_1-1"></div>
            <div class="line_2-2"></div>
            <div class="line_3-3"></div>
          </div>
          <div class="col-sm-1 col-lg-2"></div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> <strong> 1. </strong>Online Classes </button>
                  </h5>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingTwo">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> <strong>2.</strong> Home Tution</button>
                  </h5>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> <strong>3.</strong> Wedding Classes </button>
                  </h5>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> <strong></strong></button>
                  </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="gallery-section" class="padding-50 gray projects">
    <div class="container">
      <div class="row">
        <div class="col-sm-1 col-lg-2"></div>
        <div class="col-xs-12 col-sm-10 col-lg-8 text-center">
          <h2 class="text-uppercase title-style01">Gallery</span></h2>
          <div class="line_1-1"></div>
          <div class="line_2-2"></div>
          <div class="line_3-3"></div>
        </div>
        <div class="col-sm-1 col-lg-2"></div>
      </div>
      <div class="row pt-40 gallery_item">
        <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery Event1 mix"> <!-- Single Project -->
          <div class="project-single">
            <div class="project-inner">
              <div class="project-head"> <img src="{{@asset("images/500/33.jpg")}}" alt="#"> </div>
              <div class="button"> <a class="gallery_img btn" href="{{@asset("images/50033.jpg")}}"><i class="fa fa-photo"></i></a></div>
            </div>
          </div>
          <!--/ End Single Project --> 
        </div>
        <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery Event4 mix"> <!-- Single Project -->
          <div class="project-single">
            <div class="project-inner">
              <div class="project-head"> <img src="{{@asset("images/500/34.jpg")}}" alt="#"> </div>
             
              <div class="button"> <a class="gallery_img btn" href="{{@asset("images/500/34.jpg")}}"><i class="fa fa-photo"></i></a> </div>
            </div>
          </div>
          <!--/ End Single Project --> 
        </div>
        <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery Event2 mix"> <!-- Single Project -->
          <div class="project-single">
            <div class="project-inner">
              <div class="project-head"> <img src="{{@asset("images/500/6.jpg")}}" alt="#"> </div>
             
              <div class="button"> <a href="{{@asset("images/500/6.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
            </div>
          </div>
          <!--/ End Single Project --> 
        </div>
        <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
          
          <div class="project-single">
            <div class="project-inner">
              <div class="project-head"> <img src="{{@asset("images/500/2.jpg")}}" alt="#"> </div>
             
              <div class="button"> <a href="{{@asset("images/500/2.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a> </div>
            </div>
          </div>
          <!--/ End Single Project --> 
        </div>
        <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
          <div class="project-single">
            <div class="project-inner">
              <div class="project-head"> <img src="{{@asset("images/500/8.jpg")}}" alt="#"> </div>
             
             
              <div class="button"> <a href="{{@asset("images/500/8.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
            </div>
          </div>
          <!--/ End Single Project --> 
        </div>
        <div class="col-lg-4 col-sm-6 col-xs-12 main-gallery"> <!-- Single Project -->
          <div class="project-single">
            <div class="project-inner">
              <div class="project-head"> <img src="{{@asset("images/500/7.jpg")}}" alt="#"> </div>
            
              <div class="button"> <a href="{{@asset("images/500/7.jpg")}}" class="btn gallery_img"><i class="fa fa-photo"></i></a></div>
            </div>
          </div>
          <!--/ End Single Project --> 
        </div>
      </div>
    </div>
  </div>

  <div id="contacts" class="padding-50 gray">
    <div class="container">
      <div class="row">
        <div class="col-sm-1 col-lg-2"></div>
        <div class="col-xs-12 col-sm-10 col-lg-8 text-center">
          <h2 class="text-uppercase title-style01">Contact<span class="color_red"> Us</span></h2>
          <div class="line_1-1"></div>
          <div class="line_2-2"></div>
          <div class="line_3-3"></div>
          <p class="heading_space">Fill the form and we wil get back to you as soon as possible </p>
        </div>
        <div class="col-sm-1 col-lg-2"></div>
      </div>
      <div class="row pt-40">
        <div class="col-lg-6 col-sm-12">
          <div class="contact-info">
            <h5>Main Office</h5>
            <div class="contact-inner-box">
              <div class="contact-inner-icon"> <i class="fa fa-map-marker"></i> </div>
              <div class="contact-inner-text"> <span>Main Gate, South of Gandhi Maidan
              </span> <span>Masaurhi, 804452</span> <span>Patna, Bihar
              </span> </div>
            </div>
            <div class="contact-inner-box">
              <div class="contact-inner-icon"> <i class="fa fa-envelope-o" aria-hidden="true"></i> </div>
              <div class="contact-inner-text"> <span>www.adityadancestudio.net.in</span> <span>adityakumar12091981@gmail.com</span> </div>
            </div>
            <div class="contact-inner-box">
              <div class="contact-inner-icon"> <i class="fa fa-phone"></i> </div>
              <div class="contact-inner-text"> <span>+ 91-7488087155 </span> </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="contact-info">
            <h5>Branch Office</h5>
            <div class="contact-inner-box">
              <div class="contact-inner-icon"> <i class="fa fa-map-marker"></i> </div>
              <div class="contact-inner-text"> <span>Street beside the Photo Bharat </span> <span>Station Road, Masaurhi, 804452
              </span> <span>Patna, Bihar</span> </div>
            </div>
            <div class="contact-inner-box">
              <div class="contact-inner-icon"> <i class="fa fa-envelope-o" aria-hidden="true"></i> </div>
              <div class="contact-inner-text"> <span>www.adityadancestudio.net.in</span> <span>adityakumar12091981@gmail.com</span> </div>
            </div>
            <div class="contact-inner-box">
              <div class="contact-inner-icon"> <i class="fa fa-phone"></i> </div>
              <div class="contact-inner-text"> <span>+ 91-7488087155 </span> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row pt-40">
        <div class="col-lg-12 col-sm-12">
          <div class="contact">
            <form class="form" name="enq" method="post" action="" onSubmit="return validation();">
              <div class="row">
                <div class="form-group col-md-6">
                  <input name="name" class="form-control" id="firstname" placeholder="Name" required="required" type="text">
                </div>
                <div class="form-group col-md-6">
                  <input name="email" class="form-control" id="email" placeholder="Email" required="required" type="email">
                </div>
                <div class="form-group col-md-12">
                  <input name="subject" class="form-control" id="subject" placeholder="Subject" required="required" type="text">
                </div>
                <div class="form-group col-md-12">
                  <textarea rows="3" name="message" class="form-control" id="description" placeholder="Your Message" required="required"></textarea>
                </div>
                <div class="col-md-12">
                  <div class="actions">
                    <input value="Send message" name="submit" id="submit-Button" class="btn btn-lg btn-con-bg" title="Submit Your Message!" type="submit">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection