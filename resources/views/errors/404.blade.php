@extends('layout')
@section('title') 
    404
@endsection
@section('body')
<!-- Error Section -->
<div id="blog-section" class="padding-50 white blog-section blogSectionInn">
    <div class="container">
      
      <div class="row">
       
       
     <div class="error-box">
    <div class="error-box-text">
      <h1>404</h1>
      <h3>Page not Found</h3>
      <h4>We're sorry, but the page you were looking for doesn't exist.</h4>
      <div class="mt-30"><a href="/" class="button-md dark-button">Back Home</a></div>
    </div>
  </div>
    

      </div>
      
    </div>
  </div>
  <!-- Error Section --> 
@endsection